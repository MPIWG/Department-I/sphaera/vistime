# De sphaera 

Tool to explore the data of MPI's project "The Sphere: Knowledge System Evolution and the Shared Scientific Identity in Europe"

## Getting started


#### Requirements

- `node@>=15.0.0`
- `npm@>=7.0.0`

#### Install dependencies 

```sh
npm install
```

#### Mapbox Api Access Token

In order to run the map with you need a mapbox access token.

Create a `.env.local` file in `/src` directory. With the following content:

```
NEXT_PUBLIC_MAPBOX_API_ACCESS_TOKEN=<YOUR_TOKEN_HERE>
```

- [More information about Mapbox Tokens and Alternarnatives](https://visgl.github.io/react-map-gl/docs/get-started/mapbox-tokens)
- [More information on NextJS Environment Variables](https://nextjs.org/docs/basic-features/environment-variables#loading-environment-variables)

## Running the project

```
npm run dev
```
