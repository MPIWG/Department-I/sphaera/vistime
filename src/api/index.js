const API_BASE_URL = "http://0.0.0.0:3000/";
const API_BASE_PATH = `data/`;

const loadData = (file) => fetch(`${API_BASE_URL}${API_BASE_PATH}${file}`);

export const loadJson = async (file) => {
  const res = await loadData(file);
  const json = await res.json();
  return json;
};

const getIndexedChainFilename = (index) => `FP Chain ${index}.json`;
const MAX = 56;

export const loadIndividualChains = async () => {
  const chains = await Promise.all(
    Array(MAX)
      .fill(null)
      .map((_, i) => loadJson(getIndexedChainFilename(i + 1)))
  );
  return chains;
};
