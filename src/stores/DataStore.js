import Places from "./modules/Places";
import Timespans from "./modules/Timespans";
import Editions from "./modules/Editions";
import Chains from "./modules/Chains";

class DataStore {
  constructor() {
    this.places = new Places();
    this.timespans = new Timespans();
    this.editions = new Editions(this);
    this.chains = new Chains(this);
  }

  loadData = async () => {
    await Promise.all([this.places.loadData(), this.timespans.loadData()]);
    await this.editions.loadData();
    await this.chains.loadData();
  };

  toJSON = () => {
    return {
      places: this.places.toJSON(),
      timespans: this.timespans.toJSON(),
      editions: this.editions.toJSON(),
      chains: this.chains.toJSON(),
    };
  };
  hydrate = (snapshot) => {
    const { places, timespans, editions, chains } = snapshot;
    this.places.hydrate(places);
    this.timespans.hydrate(timespans);
    this.editions.hydrate(editions);
    this.chains.hydrate(chains);
  };
}

export default DataStore;
