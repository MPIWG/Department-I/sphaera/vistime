export const filterBooksByPlace = (booksByPlace, filter) =>
  Object.keys(booksByPlace).reduce((acc, place) => {
    const booksInTimeSpan = Object.keys(booksByPlace[place]).reduce(
      (acc2, fileName) => {
        acc2[fileName] = booksByPlace[place][fileName].filter(filter);
        return acc2;
      },
      {}
    );
    acc[place] = booksInTimeSpan;
    return acc;
  }, {});
