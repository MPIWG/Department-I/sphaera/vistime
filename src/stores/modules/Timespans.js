import { loadJson } from "../../api";
import Timespan from "../models/Timespan";
import { uniqBy, maxBy, minBy } from "lodash";
import { makeObservable, observable, computed, action, flow } from "mobx";

class Timespans {
  constructor() {
    this.asList = [];
    makeObservable(this, {
      asList: observable,
      loadData: action,
      byEditionId: computed,
      extent: computed,
    });
  }

  toJSON = () => {
    return {
      asList: this.asList,
    };
  };

  hydrate = (snapshot) => {
    this.asList = snapshot.asList.map((timespan) => new Timespan(timespan));
  };

  loadData = async () => {
    const timespans = await loadJson(`timespans.json`);
    this.asList = timespans.map((timespan) => Timespan.fromApi(timespan));
  };

  get byEditionId() {
    return uniqBy(this.asList, (timespan) => timespan.editionId).reduce(
      (acc, timespan) => {
        acc[timespan.editionId] = timespan;
        return acc;
      },
      {}
    );
  }

  get extent() {
    const min = minBy(this.asList, (t) => t.from);
    const max = maxBy(this.asList, (t) => t.from);
    return [min.from, max.from];
  }
}

export default Timespans;
