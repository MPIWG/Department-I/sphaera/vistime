import { loadJson } from "../../api";
import Edition from "../models/Edition";
import { uniqBy } from "lodash";
import { makeObservable, observable, computed, action, flow } from "mobx";

class Editions {
  constructor(parent) {
    this.timespans = parent.timespans;
    this.places = parent.places;
    this.asList = [];
    makeObservable(this, {
      asList: observable,
      timespans: observable,
      places: observable,
      loadData: action,
      byId: computed,
    });
  }

  toJSON = () => {
    return {
      asList: this.asList,
    };
  };

  hydrate = (snapshot) => {
    this.asList = snapshot.asList.map(
      (edition) =>
        new Edition({
          ...edition,
          place: this.places.byLabel[edition.place.label],
          timespan: this.timespans.byEditionId[edition.id],
        })
    );
  };

  loadData = async () => {
    const books = await loadJson(`books.json`);
    this.asList = books.map((edition) =>
      Edition.fromApi({
        ...edition,
        place: this.places.byLabel[edition.place],
        timespan: this.timespans.byEditionId[edition.id],
      })
    );
  };

  get byId() {
    return uniqBy(this.asList, (edition) => edition.id).reduce(
      (acc, edition) => {
        acc[edition.id] = edition;
        return acc;
      },
      {}
    );
  }
}

export default Editions;
