import { loadIndividualChains } from "../../api";
import FingerPrint from "../models/FingerPrint";
import Chain from "../models/Chain";
import { uniqBy } from "lodash";
import { makeObservable, observable, computed, action, flow } from "mobx";

class Chains {
  constructor(parent) {
    this.editions = parent.editions;
    this.asList = [];
    makeObservable(this, {
      asList: observable,
      loadData: action,
      byFileName: computed,
      chainForEdition: computed,
      fingerprintForEdition: computed,
    });
  }

  toJSON = () => {
    return {
      asList: this.asList,
    };
  };

  hydrate = (snapshot) => {
    this.asList = snapshot.asList.map(
      (chain, index) =>
        new Chain({
          index,
          fingerprints: chain.fingerprints.map(
            (fp) =>
              new FingerPrint({
                from: this.editions.byId[fp.from.id],
                to: this.editions.byId[fp.to.id],
              })
          ),
        })
    );
  };

  loadData = async () => {
    const chains = await loadIndividualChains();
    this.asList = chains.map(
      (chain, index) =>
        new Chain({
          index,
          fingerprints: chain.map((fp) =>
            FingerPrint.fromApi({
              e1: this.editions.byId[fp.e1],
              e2: this.editions.byId[fp.e2],
            })
          ),

        })
    );
  };

  get byFileName() {
    return this.asList.reduce((acc, chain) => {
      acc[chain.fileName] = chain;
      return acc;
    }, {});
  }

  get chainForEdition() {
    return (e) => this.asList.find((chain) => chain.flattend.includes(e));
  }

  get fingerprintForEdition() {
    return (e) => {
      const chainOfEdition = this.chainForEdition(e);
      const fromFingerPrint = chainOfEdition?.fingerprints.find(
        (fp) => fp.from === e
      );
      return fromFingerPrint;
    };
  }
}

export default Chains;
