import { loadJson } from "../../api";
import getRandomColor from "../../utils/getRandomColor";
import { CitiesColorPalette } from "../models/ColorPalette";
import Place from "../models/Place";
import { maxBy, minBy, uniqBy } from "lodash";
import { makeObservable, observable, computed, action, flow } from "mobx";

class Places {
  constructor() {
    this.asList = [];
    makeObservable(this, {
      asList: observable,
      loadData: action,
      byLabel: computed,
      coordinatesExtent: computed,
    });
  }

  toJSON = () => {
    return {
      asList: this.asList,
    };
  };

  hydrate = (snapshot) => {
    this.asList = snapshot.asList.map((place) => new Place({ ...place }));
  };

  loadData = async () => {
    const places = await loadJson(`places.json`);
    this.asList = places.map((place, index) =>
      Place.fromApi({ ...place, color: CitiesColorPalette[index] })
    );
  };

  get byLabel() {
    return uniqBy(this.asList, (p) => p.label).reduce((acc, p) => {
      acc[p.label] = p;
      return acc;
    }, {});
  }

  get coordinatesExtent() {
    const minLat = minBy(this.asList, (p) => p.lat);
    const maxLat = maxBy(this.asList, (p) => p.lat);
    const minLong = minBy(this.asList, (p) => p.long);
    const maxLong = maxBy(this.asList, (p) => p.long);
    return {
      lat: [minLat.lat, maxLat.lat],
      long: [minLong.long, maxLong.long],
    };
  }
}

export default Places;
