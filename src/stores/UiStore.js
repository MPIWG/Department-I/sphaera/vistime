import { makeObservable, observable, computed, action, flow } from "mobx";

class UiStore {
  _selectedTimeRange = null;
  focusedChain = null;
  focusedEdition = null;
  selectedChain = null;
  isPlaying = false;
  _selectedYear = null;
  isPaused = false;
  excludedChains = [];
  constructor(props) {
    makeObservable(this, {
      excludedChains: observable,
      addExcludedChain: action,
      removeExcludedChain: action,
      toggleChainExclusion: action,
      excludeAllChains: action,
      includeAllChains: action,
      _selectedTimeRange: observable,
      selectedTimeRange: computed,
      setSelectedTimeRange: action,
      _selectedYear: observable,
      selectedYear: computed,
      setSelectedYear: action,
      incrementSelectedYear: action,
      selectedChain: observable,
      setSelectedChain: action,
      focusedEdition: observable,
      setFocusedEdition: action,
      focusedChain: observable,
      setFocusedChain: action,
      isPlaying: observable,
      toggleIsPlaying: action,
      isPaused: observable,
      toggleIsPaused: action,
      setIsPaused: action,
    });
  }

  connectStore = ({ dataStore }) => {
    this.dataStore = dataStore;
  };

  excludeAllChains() {
    this.excludedChains = this.dataStore.chains.asList;
  }

  includeAllChains() {
    this.excludedChains = [];
  }

  toggleChainExclusion(chain) {
    if (this.excludedChains.includes(chain)) {
      this.removeExcludedChain(chain);
    } else {
      this.addExcludedChain(chain);
    }
  }
  addExcludedChain(chain) {
    this.excludedChains.push(chain);
  }

  removeExcludedChain(chain) {
    this.excludedChains = this.excludedChains.filter((c) => c !== chain);
  }

  get selectedTimeRange() {
    return this._selectedTimeRange || this.dataStore.timespans.extent;
  }

  setSelectedTimeRange(newRange) {
    this._selectedTimeRange = newRange;
  }

  get selectedYear() {
    return this._selectedYear || this.selectedTimeRange[0];
  }

  setSelectedYear(year) {
    this._selectedYear = year;
  }

  incrementSelectedYear() {
    if (this.selectedYear >= this.selectedTimeRange[1]) {
      this._selectedYear = this.selectedTimeRange[0];
    } else {
      this._selectedYear = this.selectedYear + 1;
    }
  }

  setSelectedChain(chain) {
    this.selectedChain = chain;
  }

  setFocusedChain(chain) {
    this.focusedChain = chain;
  }
  setFocusedEdition(edition) {
    this.focusedEdition = edition;
  }
  toggleIsPlaying() {
    this.isPlaying = !this.isPlaying;
  }

  setIsPaused(state) {
    this.isPaused = state;
  }
  toggleIsPaused() {
    this.isPaused = !this.isPaused;
  }
}

export default UiStore;
