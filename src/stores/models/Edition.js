import { makeObservable, observable, computed, action, flow } from "mobx";

class Edition {
  constructor(props) {
    this.id = String(props.id);
    this.label = String(props.label);
    this.yearPublished = Number(props.yearPublished);
    this.timespan = props.timespan;
    this.place = props.place;
    this.url = props.url;
    this.format = props.format;
    this.author = props.author;
    this.publisher = props.publisher;
    this.printer = props.printer;
    this.fingerprint = props.fingerprint;
    makeObservable(this, {
      fullLabel: computed,
      href: computed,
    });
  }
  static fromApi(props) {
    return new Edition({
      id: props.custom_identifier,
      label: props.label,
      yearPublished: props.year,
      timespan: props.timespan,
      place: props.place,
      url: props.url,
      author: props.author,
      publisher: props.publisher,
      printer: props.printer,
      format: props.format,
      fingerprint: props.Fingerprint,
    });
  }

  get fullLabel() {
    return `${this.label}, ${this.place.label} [${this.yearPublished}] | ${this.timespan.from} - ${this.timespan.to}`;
  }

  get href() {
    return `https://${this.url}`;
  }
}

export default Edition;
