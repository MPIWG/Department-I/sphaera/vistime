import { makeObservable, observable, computed, action, flow } from "mobx";
import * as turf from "@turf/turf";

class FingerPrint {
  constructor(props) {
    this.from = props.from;
    this.to = props.to;
    makeObservable(this, {
      distance: computed,
      isOutgoing: computed,
      publishingInterval: computed,
      speedOfSpread: computed,
      speedOfSpreadRelativeToPublishingInterval: computed,
    });
  }
  static fromApi(props) {
    return new FingerPrint({
      from: props.e1,
      to: props.e2,
    });
  }
  get distance() {
    const fromPoint = turf.point(this.from.place.coordinates);
    const toPoint = turf.point(this.to.place.coordinates);
    return turf.distance(fromPoint, toPoint);
  }

  get isOutgoing() {
    return this.to.place.label !== this.from.place.label;
  }

  get publishingInterval() {
    return Math.abs(this.to.yearPublished - this.from.yearPublished);
  }

  get speedOfSpread() {
    if (this.publishingInterval === 0) return this.distance;
    return this.distance / this.publishingInterval;
  }

  get speedOfSpreadRelativeToPublishingInterval() {
    if (this.publishingInterval === 0) return 1;
    //  if (!this.isOutgoing) return 1;
    return 1 / this.publishingInterval;
  }
}

export default FingerPrint;
