import { makeObservable, observable, computed, action, flow } from "mobx";

class Place {
  constructor(props) {
    this.label = String(props.label);
    this.long = Number(props.long);
    this.lat = Number(props.lat);
    this.color = String(props.color);
    makeObservable(this, {
      coordinates: computed,
    });
  }
  static fromApi(props) {
    return new Place({
      label: props.place,
      long: props.longitude,
      lat: props.latitude,
      color: props.color,
    });
  }

  get coordinates() {
    return [this.long, this.lat];
  }
}

export default Place;
