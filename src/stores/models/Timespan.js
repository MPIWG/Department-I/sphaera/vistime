class Timespan {
  constructor(props) {
    this.editionId = String(props.editionId);
    this.from = Number(props.from);
    this.to = Number(props.to);
  }

  static fromApi(props) {
    return new Timespan({
      editionId: props.custom_identifier,
      from: props.t1,
      to: props.t2,
    });
  }
}

export default Timespan;
