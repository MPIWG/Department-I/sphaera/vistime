import { makeObservable, observable, computed, action, flow } from "mobx";
import { minBy, maxBy, uniq } from "lodash";
import { ChainsColorPalette } from "./ColorPalette";
import theme, { PRIMARY_MAIN } from "../../theme/";

const getChainFileName = (index) => `FP CHAIN ${index}.json`;

class Chain {
  constructor(props) {
    this.fingerprints = props.fingerprints;
    this.index = props.index;
    this.color = ChainsColorPalette[props.index];
    makeObservable(this, {
      fileName: computed,
      timeExtent: computed,
      textColor: computed,
      isOutgoing: computed,
      fillColor: computed,
      speedOfSpread: computed,
      speedOfSpreadRelativeToPublishingInterval: computed,
      frequency: computed,
      totalDistanceTravelled: computed,
      averageChainSpeed: computed,
      actualTimeExtent: computed
    });
  }

  get fileName() {
    return getChainFileName(this.index + 1);
  }

  get flattend() {
    return this.fingerprints.reduce((acc, fp, i) => {
      if (i === 0) {
        acc.push(fp.from);
      }
      acc.push(fp.to);
      return acc;
    }, []);
  }

  get timeExtent() {
    const min = minBy(this.flattend, (t) => t.timespan.from);
    const max = maxBy(this.flattend, (t) => t.timespan.to);
    return [min.timespan.from, max.timespan.to];
  }

  get textColor() {
    // TODO: its a workaround because getContrastText
    // doesnt take custom color into account
    // https://github.com/mui-org/material-ui/issues/29730
    const MUI_BLACK = "rgba(0, 0, 0, 0.87)";
    const contrastText = theme.palette.getContrastText(this.fillColor);
    return theme.palette.common.white;
    // return contrastText === MUI_BLACK
    //   ? PRIMARY_MAIN
    // : theme.palette.common.white;
  }

  get isOutgoing() {
    return uniq(this.flattend.map((e) => e.place.label)).length > 1;
  }

  get fillColor() {
    return this.isOutgoing ? this.color : PRIMARY_MAIN;
  }

  get speedOfSpread() {
    const sum = this.fingerprints.reduce((acc, f) => acc + f.speedOfSpread, 0);
    return sum / this.fingerprints.length;
  }
  get speedOfSpreadRelativeToPublishingInterval() {
    const sum = this.fingerprints.reduce(
      (acc, f) => acc + f.speedOfSpreadRelativeToPublishingInterval,
      0
    );
    return sum / this.fingerprints.length;
  }


    get totalDistanceTravelled() {
    const sum = this.fingerprints.reduce((acc, f) => acc + f.distance, 0);
    return sum;
  }

    get actualTimeExtent() {
    const sum = this.fingerprints.reduce((acc, f) => acc + f.publishingInterval, 0);
    return sum;
  }

    get averageChainSpeed() {
      const distance = this.fingerprints.reduce((acc, f) => acc + f.distance, 0);
      const duration = this.fingerprints.reduce((acc, f) => acc + f.publishingInterval, 0);
      return distance/duration;
  }


  get frequency() {
    return (
      this.fingerprints.reduce((acc, f) => acc + f.publishingInterval, 0) /
      this.fingerprints.length
    );
  }
}

export default Chain;
