import { makeObservable, observable, computed, action, flow } from "mobx";
import isBetween from "../utils/isBetween";

class ComputedStore {
  constructor(props) {
    makeObservable(this, {
      chainsByPlace: computed,
      filteredChainsByPlace: computed,
      offsetByPlaceAndChain: computed,
      chainCountByPlace: computed,
      filteredEditions: computed,
      nextEdition: computed,
      prevEdition: computed,
    });
  }
  connectStore = ({ dataStore, uiStore }) => {
    this.dataStore = dataStore;
    this.uiStore = uiStore;
  };

  get nextEdition() {
    const { focusedChain, focusedEdition } = this.uiStore;
    if (!focusedChain || !focusedEdition) return null;
    const currentEditionIndex = focusedChain.flattend.indexOf(focusedEdition);
    if (currentEditionIndex === -1) return null;
    if (currentEditionIndex === focusedChain.flattend.length - 1) {
      return focusedChain.flattend[0];
    } else {
      return focusedChain.flattend[currentEditionIndex + 1];
    }
  }

  get prevEdition() {
    const { focusedChain, focusedEdition } = this.uiStore;
    if (!focusedChain || !focusedEdition) return null;
    const currentEditionIndex = focusedChain.flattend.indexOf(focusedEdition);
    if (currentEditionIndex === -1) return null;
    if (currentEditionIndex === 0) {
      return focusedChain.flattend[focusedChain.flattend.length - 1];
    } else {
      return focusedChain.flattend[currentEditionIndex - 1];
    }
  }

  get offsetByPlaceAndChain() {
    return (fileName, place) => {
      const chains = this.chainsByPlace[place];
      return Object.keys(chains).indexOf(fileName);
    };
  }

  get chainCountByPlace() {
    return Object.keys(this.chainsByPlace).reduce((acc, place) => {
      acc[place] = Object.keys(this.chainsByPlace[place]).length;
      return acc;
    }, {});
  }

  get filteredEditions() {
    return Object.keys(this.chainsByPlace).reduce((acc, place) => {
      const chainsInPlace = this.chainsByPlace[place];
      Object.keys(chainsInPlace).forEach((fileName) => {
        const chain = chainsInPlace[fileName];
        chain.forEach((e) => {
          acc.push(e);
        });
      });
      return acc;
    }, []);
  }

  get chainsByPlace() {
    const { selectedTimeRange, excludedChains } = this.uiStore;
    const [minSelectedYear, maxSelectedYear] = selectedTimeRange;
    return this.dataStore.chains.asList.reduce((acc, chain, ii) => {
      const fileName = chain.fileName;
      const [minYear, maxYear] = chain.timeExtent;
      const isChainExluded = excludedChains.includes(chain);
      if (isChainExluded) return acc;
      chain.flattend.forEach((edition) => {
        const place = edition.place.label;
        const isInSelectedTimeRange = isBetween(
          edition.yearPublished,
          minSelectedYear,
          maxSelectedYear
        );
        if (isInSelectedTimeRange) {
          if (!acc[place]) acc[place] = {};
          if (!acc[place][fileName]) acc[place][fileName] = [];
          acc[place][fileName].push(edition);
        }
      });
      return acc;
    }, {});
  }

  get filteredChainsByPlace() {
    return Object.keys(this.chainsByPlace).reduce((acc, place) => {
      const editionsInTimeSpan = Object.keys(this.chainsByPlace[place]).reduce(
        (acc2, fileName) => {
          const relevantEditions = this.chainsByPlace[place][fileName].filter(
            (edition) =>
              isBetween(
                this.uiStore.selectedYear,
                edition.timespan.from,
                edition.timespan.to
              )
          );
          if (relevantEditions.length > 0) {
            acc2[fileName] = relevantEditions;
          }
          return acc2;
        },
        {}
      );
      acc[place] = editionsInTimeSpan;
      return acc;
    }, {});
  }

  get filteredChainsByPlaceEditionsSum() {
    return Object.keys(this.filteredChainsByPlace).reduce((accPlace, place) => {
      const allEditionsSum = Object.keys(
        this.filteredChainsByPlace[place]
      ).reduce((sum, chain) => {
        return (sum += this.filteredChainsByPlace[place][chain].length);
      }, 0);

      accPlace[place] = allEditionsSum;
      return accPlace;
    }, {});
  }
}

export default ComputedStore;
