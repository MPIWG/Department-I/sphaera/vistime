import * as React from "react";
import Box from "@mui/material/Box";
import MapGL from "../components/MapGL";
import { getStoreInstances } from "../stores/index";
import AutoSizer from "react-virtualized-auto-sizer";
import { observer } from "mobx-react";
import Header from "../components/Header";

function Index(props) {
  return (
    <>
      <Box height="100%" display="flex" flexDirection="column">
        <Header />
        <Box flexGrow={1}>
          <AutoSizer>
            {({ height, width }) => {
              return <MapGL width={width} height={height} />;
            }}
          </AutoSizer>
        </Box>
      </Box>
      <Box></Box>
    </>
  );
}

export default observer(Index);
const stupidify = (data) => JSON.parse(JSON.stringify(data));

export async function getServerSideProps(context) {
  const { dataStore } = getStoreInstances();
  await dataStore.loadData();

  return {
    props: {
      data: {
        dataStore: stupidify(dataStore.toJSON()),
      },
    },
  };
}
