const isBetween = (v, min, max) => {
  if (v >= min && v <= max) return true;
  return false;
};
export default isBetween;
