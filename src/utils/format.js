const format = (n, options = {}) => {
  const { suffix } = options;
  return `${n?.toLocaleString()}${suffix ? ` ${suffix}` : ""}`;
};
export default format;
