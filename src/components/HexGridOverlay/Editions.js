import React from "react";
import * as PIXI from "pixi.js";
import Edition from "./Edition";

const Editions = (props) => {
  const {
    focusedEdition,
    places,
    chainsByPlace,
    filteredChainsByPlace,
    createEditionPointerOverHandler,
    createEditionPointerOutHandler,
    createEditionPointerUpHandler,
    selectedChain,
    focusedChain,
    chains,
  } = props;
  return Object.keys(places).map((place) => {
    const chainsInPlace = chainsByPlace[place];
    const filteredChainsInPlace = filteredChainsByPlace[place];
    const noChainActive = Object.keys(filteredChainsInPlace).every(
      (key) => filteredChainsInPlace.length === 0
    );
    const { placeGrid } = places[place];
    return Object.keys(chainsInPlace).map((fileName, chainIndex) => {
      const chainInPlace = chainsInPlace[fileName];
      const filteredChainInPlace = filteredChainsInPlace[fileName];
      const chain = chains.byFileName[fileName];
      const hide =
        (selectedChain && selectedChain !== chain) ||
        noChainActive ||
        (focusedChain && focusedChain !== chain && selectedChain !== chain);
      return chainInPlace.map((edition, editionIndex) => {
        const isActive = filteredChainInPlace?.includes(edition);
        const point = placeGrid.start
          .add({ x: editionIndex, y: chainIndex })
          .toPoint();
        return (
          <Edition
            key={edition.id}
            isFocused={focusedEdition === edition}
            cursor="pointer"
            x={point.x}
            y={point.y}
            size={isActive ? 5 : 1}
            fill={PIXI.utils.string2hex(chain.fillColor)}
            alpha={hide ? 0.2 : 1}
            pointerover={createEditionPointerOverHandler({ edition, chain })}
            pointerout={createEditionPointerOutHandler({ edition, chain })}
            pointerup={createEditionPointerUpHandler({ edition, chain })}
          />
        );
      });
    });
  });
};
export default Editions;
