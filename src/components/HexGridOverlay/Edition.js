import React from "react";
import { CustomPIXIComponent, Container } from "react-pixi-fiber";
import * as PIXI from "pixi.js";
import Animated from "animated";

const TYPE = "Rect";
export const behavior = {
  customDisplayObject: (props) => new PIXI.Graphics(),
  customApplyProps: function (instance, oldProps = {}, newProps) {
    const { fill, x, y, size, isFocused, ...newPropsRest } = newProps;
    const {
      fill: oldFill,
      x: oldX,
      y: oldY,
      size: oldSize,
      isFocused: oldIsFocused,
      ...oldPropsRest
    } = oldProps;
    if (typeof oldProps !== "undefined") {
      instance.clear();
    }
    if (isFocused) {
      instance.beginFill(0xf57c00);
      instance.drawCircle(x, y, size + 3);
      instance.endFill();
    }
    instance.beginFill(fill);
    instance.drawCircle(x, y, size);
    instance.endFill();
    instance.dr;

    this.applyDisplayObjectProps(oldPropsRest, newPropsRest);
  },
};

const Rectangle = CustomPIXIComponent(behavior, TYPE);

PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

const AnimatedSprite = Animated.createAnimatedComponent(Rectangle);

const AnimatedBunnyFun = (props) => {
  const { size, alpha, pointerover, pointerout, pointerup, x, y, ...rest } =
    props;
  const scaleVal = React.useRef(new Animated.Value(size));
  const alphaVal = React.useRef(new Animated.Value(alpha));

  React.useEffect(() => {
    Animated.spring(scaleVal.current, {
      toValue: size,
    }).start();
  }, [size]);

  React.useEffect(() => {
    Animated.spring(alphaVal.current, {
      toValue: alpha,
    }).start();
  }, [alpha]);

  return (
    <Container
      interactive
      x={x}
      y={y}
      pointerover={pointerover}
      pointerout={pointerout}
      pointerup={pointerup}
      cursor="pointer"
    >
      <AnimatedSprite
        anchor={0.5}
        size={scaleVal.current}
        alpha={alphaVal.current}
        x={0}
        y={0}
        {...rest}
      />
    </Container>
  );
};

export default AnimatedBunnyFun;
