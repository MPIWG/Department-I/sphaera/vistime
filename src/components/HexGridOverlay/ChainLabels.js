import React from "react";
import Typography from "@mui/material/Typography";
import { observer } from "mobx-react";
import { useStores } from "../../stores/";

const ChainLabels = (props) => {
  const { places, chainsByPlace } = props;
  const { dataStore, uiStore, computedStore } = useStores();
  const { chains } = dataStore;
  const { selectedChain, focusedChain } = uiStore;
  const { filteredChainsByPlace } = computedStore;
  return Object.keys(places).map((place) => {
    const chainsInPlace = chainsByPlace[place];
    const filteredChainsInPlace = filteredChainsByPlace[place];
    const chainKeys = Object.keys(chainsInPlace);
    const { placeGrid } = places[place];
    return chainKeys.map((fileName, chainIndex) => {
      const chain = chains.byFileName[fileName];
      const chainPoint = placeGrid.start
        .add({ x: -1, y: chainIndex - 1 })
        .toPoint();
      const isVisible = focusedChain
        ? focusedChain === chain
        : Object.keys(filteredChainsInPlace).includes(chain.fileName);
      return (
        <Typography
          key={chain.index + 1}
          sx={{
            fontSize: 10,
            position: "absolute",
            left: chainPoint.x,
            top: chainPoint.y,
            transform: "translate(-100%, 3px)",
            opacity: isVisible ? 1 : 0.2,
            transition: "ease-in-out 300ms opacity",
            px: 0.5,
          }}
        >
          {chain.index + 1}
        </Typography>
      );
    });
  });
};

export default observer(ChainLabels);
