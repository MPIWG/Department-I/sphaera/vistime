// the sice of the hexes
export const HEX_SIZE = 7;
// space in hex units used around the bounding boxes for collision detection
export const MARGIN = 1;
// space in hex units that is the minimum placement adjustment when finding positions in the grid
export const SPACE_ADJUSTMENT = 1;
