import React from "react";
import Typography from "@mui/material/Typography";
import { observer } from "mobx-react";
import { useStores } from "../../stores/";

const PlaceMarkers = (props) => {
  const { places, chainsByPlace } = props;
  const { uiStore, computedStore } = useStores();
  const { selectedChain, focusedChain } = uiStore;
  const { filteredChainsByPlace } = computedStore;
  return Object.keys(places).map((place) => {
    const chainsInPlace = filteredChainsByPlace[place];
    const chainKeys = Object.keys(chainsInPlace);
    const { placeGrid } = places[place];
    const placePoint = placeGrid.start.toPoint();
    const hasActiveChain = chainKeys.some(
      (key) => key === selectedChain?.fileName || key === focusedChain?.fileName
    );
    const isVisible =
      selectedChain || focusedChain ? hasActiveChain : chainKeys.length > 0;
    return (
      <Typography
        key={place}
        variant="caption"
        sx={{
          position: "absolute",
          left: placePoint.x - 5,
          top: placePoint.y - 25,
          transform: "translate(-100%, 0)",
          opacity: isVisible ? 1 : 0.2,
          transition: "ease-in-out 300ms opacity",
          background: "rgba(255, 255, 255, 0.5)",
          borderRadius: 2,
          px: 0.5,
        }}
      >
        {place}
      </Typography>
    );
  });
};

export default observer(PlaceMarkers);
