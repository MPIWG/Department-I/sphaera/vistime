import * as Honeycomb from "honeycomb-grid";
import React from "react";
import { MapContext } from "react-map-gl";
import { observer } from "mobx-react";
import { useStores } from "../../stores/index";
import { HEX_SIZE } from "./constants";
import { Stage } from "react-pixi-fiber";
import Editions from "./Editions";
import { resolveColissions } from "./resolveCollisions";
import PlaceMarkers from "./PlaceMarkers";
import ChainLabels from "./ChainLabels";
import Paths from "./Paths";

export const Hex = Honeycomb.extendHex({ size: HEX_SIZE });
export const Grid = Honeycomb.defineGrid(Hex);

const HexGridOverlay = (props) => {
  const mapContext = React.useContext(MapContext);
  const { project, width, height } = mapContext.viewport;
  const { uiStore, computedStore, dataStore } = useStores();
  const { focusedEdition, focusedChain } = uiStore;
  const { places, chains } = dataStore;
  const { chainsByPlace, filteredChainsByPlace } = computedStore;
  const placesOnGrid = React.useMemo(
    () =>
      Object.keys(chainsByPlace).reduce((acc, placeLabel) => {
        const place = places.byLabel[placeLabel];
        const coordinates = place.coordinates;
        const start = project(coordinates);
        const chainsInPlace = chainsByPlace[placeLabel];
        const longestChainLength = Object.keys(chainsInPlace).reduce(
          (longestChainLength, fileName) => {
            const chain = chainsInPlace[fileName];
            if (chain.length > longestChainLength) {
              longestChainLength = chain.length;
            }
            return longestChainLength;
          },
          0
        );
        const startHex = Grid.pointToHex(start);
        acc[placeLabel] = {
          place,
          startCoordinates: start,
          placeGrid: Grid.rectangle({
            width: longestChainLength,
            height: Object.keys(chainsInPlace).length,
            start: startHex,
          }),
        };
        return acc;
      }, {}),
    [chainsByPlace, places, mapContext.viewport]
  );
  const fixedPlaces = React.useMemo(() => {
    let fps = placesOnGrid;
    let iteration = 0;
    while (iteration < 5) {
      fps = resolveColissions(fps, Grid);
      iteration += 1;
    }
    return fps;
  }, [placesOnGrid]);

  const createEditionPointerUpHandler =
    ({ edition, chain }) =>
    (e) => {
      if (focusedEdition === edition) {
        uiStore.setFocusedEdition(null);
        uiStore.setFocusedChain(null);
      } else {
        uiStore.setFocusedChain(chain);
        uiStore.setFocusedEdition(edition);
      }
    };
  const createEditionPointerOverHandler =
    ({ edition, chain }) =>
    (e) => {
      uiStore.setSelectedChain(chain);
    };
  const createEditionPointerOutHandler =
    ({ edition, chain }) =>
    (e) => {
      uiStore.setSelectedChain(null);
    };
  const resolution = window.devicePixelRatio || 1;
  return (
    <>
      <ChainLabels places={fixedPlaces} chainsByPlace={chainsByPlace} />
      <svg
        width={width}
        height={height}
        style={{ position: "absolute", top: 0, left: 0, pointerEvents: "none" }}
      >
        <Paths
          places={fixedPlaces}
          chainsByPlace={chainsByPlace}
          chains={chains}
        />
      </svg>
      <Stage
        style={{ position: "absolute", top: 0, left: 0 }}
        options={{
          antialias: true,
          transparent: true,
          width: width,
          height: height,
          resolution,
          autoDensity: true,
        }}
      >
        <Editions
          focusedEdition={focusedEdition}
          chains={chains}
          places={fixedPlaces}
          chainsByPlace={chainsByPlace}
          filteredChainsByPlace={filteredChainsByPlace}
          selectedChain={uiStore.selectedChain}
          focusedChain={uiStore.focusedChain}
          createEditionPointerUpHandler={createEditionPointerUpHandler}
          createEditionPointerOverHandler={createEditionPointerOverHandler}
          createEditionPointerOutHandler={createEditionPointerOutHandler}
        />
      </Stage>
      <PlaceMarkers places={fixedPlaces} chainsByPlace={chainsByPlace} />
    </>
  );
};

export default observer(HexGridOverlay);
