import { MARGIN, SPACE_ADJUSTMENT } from "./constants";

export const checkGridColission = (grid1, grid2) => {
  if (
    grid1.start.x - MARGIN < grid2.start.x + grid2.width + MARGIN &&
    grid1.start.x + grid1.width + MARGIN > grid2.start.x - MARGIN &&
    grid1.start.y - MARGIN < grid2.start.y + grid2.height + MARGIN &&
    grid1.height + grid1.start.y + MARGIN > grid2.start.y - MARGIN
  ) {
    return true;
  } else {
    return false;
  }
};

export const getIntersection = (grid1, grid2) => {
  return {
    x: Math.max(
      0,
      Math.min(grid1.start.x + grid1.width, grid2.start.x + grid2.width) -
        Math.max(grid1.start.x, grid2.start.x)
    ),
    y: Math.max(
      0,
      Math.min(grid1.start.y + grid1.height, grid2.start.y + grid2.height) -
        Math.max(grid1.start.y, grid2.start.y)
    ),
  };
};

export const resolveColissions = (placesOnGrid, Grid) => {
  const fixedPlaces = {};
  for (const place in placesOnGrid) {
    //if (place !== 'Frankfurt (Main)' )continue;
    const placeOnGrid = placesOnGrid[place];
    const { placeGrid } = placesOnGrid[place];
    const fixedPlacesAsList = Object.keys(fixedPlaces).map(
      (key) => fixedPlaces[key]
    );
    const colissions = fixedPlacesAsList.filter((fixedPlace) => {
      const isColliding = checkGridColission(placeGrid, fixedPlace.placeGrid);
      return isColliding;
    });
    let finalLocation = placeOnGrid;
    while (colissions.length > 0) {
      const collisionToResolve = colissions.pop();
      const intersection = getIntersection(
        placeGrid,
        collisionToResolve.placeGrid
      );
      const moveUp =
        finalLocation.placeGrid.start.y > collisionToResolve.placeGrid.start.y;
      const { start, width, height } = finalLocation.placeGrid;
      let shiftedStart;

      if (intersection.x < intersection.y) {
        shiftedStart = start.add({
          x: intersection.x + SPACE_ADJUSTMENT,
          y: 0,
        });
      } else {
        shiftedStart = start.add({
          x: 0,
          y: (intersection.y + SPACE_ADJUSTMENT) * (moveUp ? 1 : -1),
        });
      }
      finalLocation = {
        ...placeOnGrid,
        placeGrid: Grid.rectangle({
          width,
          height,
          start: shiftedStart,
        }),
      };
    }
    fixedPlaces[place] = finalLocation;
  }
  return fixedPlaces;
};
