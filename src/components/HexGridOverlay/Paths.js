import React from "react";
import { scaleLinear, linkVertical } from "d3";
import { observer } from "mobx-react";
import { useStores } from "../../stores/index";

const OFFSET = 6;

export const isOutgoingFingerprint = (fingerprint) =>
  fingerprint && fingerprint.from.place !== fingerprint.to.place;

const findChainAndEditionIndex = (edition, chainsInPlace) => {
  const { chainIndex, editionIndex } = Object.keys(chainsInPlace).reduce(
    (acc, fileName, chainIndex) => {
      const editionIndex = chainsInPlace[fileName]?.findIndex(
        (e) => e.id === edition.id
      );
      if (editionIndex > -1) {
        acc = {
          chainIndex,
          editionIndex,
        };
      }
      return acc;
    },
    {}
  );
  return { chainIndex, editionIndex };
};

const Paths = (props) => {
  const { places, chainsByPlace, chains } = props;
  const { uiStore, computedStore } = useStores();
  const { selectedYear, selectedChain, focusedChain } = uiStore;
  const { filteredChainsByPlace } = computedStore;
  return Object.keys(places).map((place) => {
    const chainsInPlace = chainsByPlace[place];
    const filteredChainsInPlace = filteredChainsByPlace[place];
    const { placeGrid } = places[place];
    return (
      <React.Fragment key={place}>
        {Object.keys(chainsInPlace).map((fileName, chainIndex) => {
          const chainInPlace = chainsInPlace[fileName];
          const filterdChainInPlace = filteredChainsInPlace[fileName];
          const chain = chains.byFileName[fileName];
          return (
            <React.Fragment key={fileName}>
              {chainInPlace.map((edition, editionIndex) => {
                const isActive = filterdChainInPlace?.includes(edition);
                const editionFingerPrint = chain.fingerprints.find(
                  (fp) => fp.from.id === edition.id
                );
                const isOutgoing = isOutgoingFingerprint(editionFingerPrint);
                // early return, we only calculate paths for outgoing connections
                if (!isOutgoing) {
                  return null;
                }

                const startHex = placeGrid.start.add({
                  x: editionIndex,
                  y: chainIndex,
                });
                const chainsInTargetPlace =
                  chainsByPlace[editionFingerPrint.to.place.label];
                const targetPlace = places[editionFingerPrint.to.place.label];
                if (!targetPlace) return null;
                const { placeGrid: targetPlaceGrid } = targetPlace;
                const {
                  chainIndex: targetChainIndex,
                  editionIndex: targetEditionIndex,
                } = findChainAndEditionIndex(
                  editionFingerPrint.to,
                  chainsInTargetPlace
                );
                const endHex = targetPlaceGrid.start.add({
                  x: targetEditionIndex,
                  y: targetChainIndex,
                });
                const startPoint = startHex.toPoint();
                const endPoint = endHex.toPoint();
                const movesUp = startPoint.y < endPoint.y;
                const movesLeft = startPoint.x > endPoint.x;
                const xOffset = movesLeft ? -OFFSET : OFFSET;
                const yOffset = movesUp ? OFFSET : -OFFSET;
                const startPointLink = [
                  startPoint.x + xOffset,
                  startPoint.y + yOffset,
                ];
                const endPointLink = [endPoint.x, endPoint.y - yOffset];
                const link = linkVertical()({
                  source: startPointLink,
                  target: endPointLink,
                });
                const d = `M${startPoint.x},${startPoint.y} L${startPointLink[0]},${startPointLink[1]}  ${link}`;
                const hide =
                  (selectedChain && selectedChain !== chain) ||
                  (focusedChain &&
                    focusedChain !== chain &&
                    selectedChain !== chain);
                const opacity = isActive ? (hide ? 0 : 1) : 0;
                const color = chain.color; //edition.place.color;
                const minTime = editionFingerPrint.from.timespan.from;
                const maxTime = editionFingerPrint.to.timespan.from;
                const offset =
                  minTime === maxTime
                    ? selectedYear >= minTime
                      ? 0
                      : 1
                    : scaleLinear()
                        .domain([minTime, maxTime])
                        .range([1, 0])
                        .clamp(true)(selectedYear);
                const key = `${editionFingerPrint.from.id}-${editionFingerPrint.to.id}`;
                const markerRot = Math.atan2(
                  endPoint.y - startPoint.y,
                  endPoint.x - startPoint.x
                );
                const path = (
                  <React.Fragment key={key}>
                    <defs>
                      <marker
                        id={key}
                        viewBox="0 0 10 10"
                        refX="7"
                        refY="4"
                        markerUnits="strokeWidth"
                        markerWidth="10"
                        markerHeight="10"
                        orient="auto-start-reverse"
                        //orient={`${markerRot}rad`}
                      >
                        <path
                          d="M 0 0 L 10 5 L 0 10 z"
                          fill={color}
                          style={{ transform: "scale(0.8)" }}
                        />
                      </marker>
                    </defs>

                    {/* curved part */}
                    <path
                      markerEnd={offset === 0 ? `url(#${key})` : null}
                      strokeWidth="1"
                      pathLength="1"
                      fill="none"
                      stroke={color}
                      d={d}
                      strokeLinecap="round"
                      strokeLinejoin="miter"
                      style={{
                        opacity: opacity,
                        strokeDasharray: 1,
                        strokeDashoffset: offset,
                        transition:
                          "stroke-dashoffset linear 300ms,  opacity ease-in-out 300ms",
                      }}
                    />
                  </React.Fragment>
                );
                return path;
              })}
            </React.Fragment>
          );
        })}
      </React.Fragment>
    );
  });
};

export default observer(Paths);
