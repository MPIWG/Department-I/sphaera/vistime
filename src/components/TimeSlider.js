import React from "react";
import { observer } from "mobx-react";
import { scaleLinear } from "d3";
import { useStores } from "../stores/index";
import useInterval from "../utils/useInterval";
import { Box, Tooltip, Fab, Slider, SliderThumb } from "@mui/material";
import PlayArrowIcon from "@mui/icons-material/PlayArrow";
import PauseIcon from "@mui/icons-material/Pause";
import { styled } from "@mui/material/styles";
import SpeedDial from "@mui/material/SpeedDial";
import SpeedDialAction from "@mui/material/SpeedDialAction";
import SpeedIcon from "@mui/icons-material/Speed";

const SpeedLabelIcon = (props) => {
  const { label } = props;

  return (
    <Box display="flex" alignItems="center">
      {label}
    </Box>
  );
};

const CustomSpeedDialAction = ({ isActive, ...props }) => (
  <SpeedDialAction
    {...props}
    FabProps={{
      ...props.FabProps,
      size: "large",
    }}
  />
);

const StyledSpeedDialAction = styled(CustomSpeedDialAction)(
  ({ isActive, theme }) => ({
    "&.MuiSpeedDialAction-fab": {
      background: isActive
        ? theme.palette.secondary.main
        : theme.palette.background.paper,
    },
  })
);

const ValueLabelComponent = React.forwardRef((props, ref) => {
  const { children, value, open } = props;
  return (
    <Tooltip placement="top" title={value} open={open} ref={ref}>
      {children}
    </Tooltip>
  );
});

ValueLabelComponent.displayName = "ValueLabelComponent";

const SelectedYearSlider = styled(Slider)(({ theme }) => ({
  "& .MuiSlider-rail": {
    background: "transparent",
  },
  "& .MuiSlider-thumb": {
    transition: "box-shadow 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
  },
}));

SelectedYearSlider.displayName = "SelectedYearSlider";

const CustomThumb = React.forwardRef((props, ref) => {
  const { children, onMouseDown, ...other } = props;
  const handleMouseDown = (e) => {
    e.stopPropagation();
  };
  return (
    <SliderThumb
      ref={ref}
      {...other}
      sx={{
        pointerEvents: "auto",
        borderRadius: 12,
        width: 15,
        height: 15,
        color: "secondary.main",
        border: 2,
        borderColor: "primary.main",
      }}
    >
      {children}
    </SliderThumb>
  );
});

CustomThumb.displayName = "CustomThumb";

const TimeSlider = (props) => {
  const { dataStore, uiStore } = useStores();
  const { selectedTimeRange, isPlaying, isPaused, selectedYear, focusedChain } =
    uiStore;
  const { timespans, places, chains } = dataStore;
  const [minTimespan, maxTimespan] = timespans.extent;

  const [animationSpeed, setAnimationSpeed] = React.useState(0.25);

  const handleClickPlayButton = () => {
    uiStore.toggleIsPlaying();
  };
  const handleChangeSelectedTimerange = (e, newValue) => {
    uiStore.setSelectedTimeRange(newValue);
  };
  const handleChangeSelectedYear = (e, newValue) => {
    uiStore.setSelectedYear(newValue);
  };

  const handleClickAnimationSpeedFull = () => {
    setAnimationSpeed(1);
  };

  const handleClickAnimationSpeedHalf = () => {
    setAnimationSpeed(0.5);
  };
  const handleClickAnimationSpeedQuarter = () => {
    setAnimationSpeed(0.25);
  };
  const xScale = scaleLinear()
    .domain([minTimespan, maxTimespan])
    .range([0, 100]);

  const [timeSliderLeft, timeSliderWidth] = React.useMemo(() => {
    const x1 = xScale(selectedTimeRange[0]);
    const x2 = xScale(selectedTimeRange[1]);
    return [x1, 100 - x1 - (100 - x2)];
  }, [selectedTimeRange]);

  useInterval(() => {
    if (!isPlaying || isPaused) return;
    uiStore.incrementSelectedYear();
  }, 1000 / (10 * animationSpeed));

  const handleStartManualSelection = (e) => {
    uiStore.setIsPaused(true);
  };

  const handleEndManualSelection = () => {
    uiStore.setIsPaused(false);
  };

  const chainPoints = React.useMemo(() => {
    const xScale = scaleLinear()
      .domain([minTimespan, maxTimespan])
      .range([0, 100]);
    return focusedChain?.flattend.map((e) => {
      const x = xScale(e.yearPublished);
      return (
        <Box
          key={e.id}
          sx={{
            position: "absolute",
            left: `${x}%`,
            top: 0,
            width: 5,
            height: 5,
            bgcolor: focusedChain.fillColor,
            transform: "translate(-50%, -50%)",
            opacity: 0.5,
          }}
        />
      );
    });
  }, [focusedChain]);
  return (
    <Box display="flex" alignItems="center">
      <Fab color="primary" onClick={handleClickPlayButton} sx={{ mr: 2 }}>
        {isPlaying ? <PauseIcon /> : <PlayArrowIcon />}
      </Fab>
      <Box sx={{ position: "relative", width: 40, height: 40, mr: 5 }}>
        <SpeedDial
          sx={{ position: "absolute", top: 0, left: 0 }}
          direction="down"
          ariaLabel="Animation Speed"
          icon={<SpeedIcon />}
        >
          <StyledSpeedDialAction
            key="quarter-speed"
            isActive={animationSpeed === 0.25}
            icon={<SpeedLabelIcon label="1x" />}
            tooltipTitle="Animation Speed x1"
            onClick={handleClickAnimationSpeedQuarter}
          />
          <StyledSpeedDialAction
            key="half-speed"
            isActive={animationSpeed === 0.5}
            icon={<SpeedLabelIcon label="2x" />}
            tooltipTitle="Animation Speed x2"
            onClick={handleClickAnimationSpeedHalf}
          />
          <StyledSpeedDialAction
            key="full-speed"
            isActive={animationSpeed === 1}
            icon={<SpeedLabelIcon label="3x" />}
            tooltipTitle="Animation Speed x3"
            onClick={handleClickAnimationSpeedFull}
          />
        </SpeedDial>
      </Box>
      <Box sx={{ position: "relative", display: "flex", flexGrow: 1 }}>
        {chainPoints}
        <Slider
          size="small"
          disabled={isPlaying}
          min={minTimespan}
          max={maxTimespan}
          getAriaLabel={() => "Temperature range"}
          value={selectedTimeRange}
          onChange={handleChangeSelectedTimerange}
          valueLabelDisplay="auto"
          disableSwap
          getAriaValueText={(v) => v}
          marks={[
            { value: minTimespan, label: minTimespan },
            { value: maxTimespan, label: maxTimespan },
          ]}
          sx={{
            marginBottom: 0,
          }}
          components={{ ValueLabel: ValueLabelComponent }}
        />
        <SelectedYearSlider
          sx={{
            top: 1,
            position: "absolute",
            left: `${timeSliderLeft}%`,
            width: `${timeSliderWidth}%`,
            pointerEvents: "none",
          }}
          track={false}
          size="small"
          min={selectedTimeRange[0]}
          max={selectedTimeRange[1]}
          getAriaLabel={() => "Temperature range"}
          value={selectedYear}
          onChange={handleChangeSelectedYear}
          valueLabelDisplay={"on"}
          getAriaValueText={(v) => v}
          onMouseDown={handleStartManualSelection}
          onChangeCommitted={handleEndManualSelection}
          components={{ Thumb: CustomThumb, ValueLabel: ValueLabelComponent }}
        />
      </Box>
    </Box>
  );
};

export default observer(TimeSlider);
