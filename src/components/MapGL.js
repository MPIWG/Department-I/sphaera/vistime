import * as React from "react";
import { useState } from "react";
import ReactMapGL, { FlyToInterpolator } from "react-map-gl";
import dynamic from "next/dynamic";
import { useStores } from "../stores/index";
import { observer } from "mobx-react";

const HexGridOverlay = dynamic(() => import("./HexGridOverlay"), {
  ssr: false,
});

const API_TOKEN = process.env.NEXT_PUBLIC_MAPBOX_API_ACCESS_TOKEN;
const MAP_STYLE_URL = "/MPI-Sphere-mono(ckybi5xwlqhk614pezjjst0am)/style.json";

const MapGL = (props) => {
  const { width, height, chainsByPlace } = props;
  const { uiStore } = useStores();
  const { focusedEdition } = uiStore;
  const [viewport, setViewport] = useState({
    width,
    height,
    latitude: 47.168504961316096,
    longitude: 10.588913643582542,
    zoom: 4.5,
  });

  // Update size when props change
  React.useEffect(() => {
    setViewport({
      ...viewport,
      width,
      height,
    });
  }, [width, height]);

  React.useEffect(() => {
    if (!focusedEdition) return;
    setViewport({
      ...viewport,
      longitude: focusedEdition?.place.long,
      latitude: focusedEdition?.place.lat,
      transitionInterpolator: new FlyToInterpolator({ speed: 1.2 }),
      transitionDuration: "auto",
    });
  }, [focusedEdition]);
  return (
    <ReactMapGL
      {...viewport}
      minZoom={4.5}
      mapboxApiAccessToken={API_TOKEN}
      mapStyle={MAP_STYLE_URL}
      onViewportChange={(nextViewport) => setViewport(nextViewport)}
    >
      <HexGridOverlay />
    </ReactMapGL>
  );
};

export default observer(MapGL);
