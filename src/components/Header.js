import * as React from "react";
import TimeSlider from "./TimeSlider";
import CloseIcon from "@mui/icons-material/Close";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import InsertLinkIcon from "@mui/icons-material/InsertLink";
import Fade from "@mui/material/Fade";
import {
  Typography,
  Box,
  Slide,
  IconButton,
  Button,
  Divider,
  Tooltip,
} from "@mui/material";
import { observer } from "mobx-react";
import { useStores } from "../stores";
import ChainFilter from "./ChainFilter";
import EditionMeta, { Value } from "./EditionMeta";
import format from "../utils/format";
const MAX_WIDTH = 240;

const Header = () => {
  const { computedStore, dataStore, uiStore } = useStores();
  const { chains } = dataStore;
  const { focusedChain, focusedEdition } = uiStore;
  const filteredCount = computedStore.filteredEditions.length;
  const allCount = dataStore.editions.asList.length;
  const isFilterActive = filteredCount !== allCount;
  const [isOpen, setIsOpen] = React.useState(focusedEdition && focusedChain);
  const handleClickNext = () => {
    const { nextEdition } = computedStore;
    if (nextEdition) {
      uiStore.setFocusedEdition(nextEdition);
      uiStore.setSelectedYear(nextEdition.yearPublished);
    }
  };
  const handleClickPrev = () => {
    const { prevEdition } = computedStore;
    if (prevEdition) {
      uiStore.setFocusedEdition(prevEdition);
      uiStore.setSelectedYear(prevEdition.yearPublished);
    }
  };
  const handleClickCloseMetaButton = () => {
    setIsOpen(false);
    setTimeout(() => {
      uiStore.setFocusedEdition(null);
      uiStore.setFocusedChain(null);
    }, 300);
  };

  const editionIndex = React.useMemo(() => {
    if (!focusedEdition || !focusedChain) return null;
    return focusedChain.flattend.indexOf(focusedEdition);
  }, [focusedEdition, focusedChain]);

  React.useEffect(() => {
    if (focusedChain && focusedEdition) {
      setIsOpen(true);
    }
  }, [focusedChain, focusedEdition]);

  return (
    <>
      <Box
        sx={{
          pointerEvents: "none",
          position: "absolute",
          width: "100%",
          height: "100%",
          top: 0,
          zIndex: 1,
          px: 4,
          pt: 4,
          pb: 5,
          display: "flex",
          flexDirection: "column",
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <Box
            sx={{
              width: MAX_WIDTH,
              p: 1,
            }}
          >
            <Box
              component="img"
              sx={{ width: 140, height: "auto", mb: 2 }}
              src="/logo.svg"
            />
            <Typography
              sx={{ fontVariantNumeric: "tabular-nums" }}
              variant="h3"
              component="h1"
              color="primary.main"
            >
              {isFilterActive && `${filteredCount}/`}
              {allCount} editions
            </Typography>
            <Divider
              sx={{ mt: 1, color: (theme) => theme.palette.primary.main }}
            />
          </Box>

          <Box
            flexGrow={1}
            p={2}
            display="flex"
            flexDirection="column"
            sx={{ pointerEvents: "auto" }}
          >
            <Box mb={3}>
              <TimeSlider />
            </Box>
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <ChainFilter />
              <Fade in={isOpen}>
                <div>








                  <Box
                    sx={{
                      ml: 4,
                      px: 1.5,
                      borderRadius: 1,
                      boxShadow: 2,
                      bgcolor: focusedChain?.fillColor,
                      color: focusedChain?.textColor,
                      opacity: 0.75,
                      height: 40, // this is the height of the button next to it...
                      display: "flex",
                      alignItems: "center",
                      overflow: "auto",
                    }}
                  >



                    <Typography
                      sx={{
                        mr: 4,
                        whiteSpace: "nowrap",

                        color: focusedChain?.textColor,
                      }}
                      color="inherit"
                      variant="h6"
                    >
                      Chain {focusedChain?.index + 1}
                    </Typography>




                    <Value
                      mb={0}
                      noWrap
                      LabelProps={{ sx: { color: "inherit" } }}
                      label={<>Duration</>}
                      value={format(focusedChain?.actualTimeExtent, {
                        suffix: "years",
                      })}
                    />&emsp;&emsp;&emsp;

                    <Value
                      mb={0}
                      noWrap
                      LabelProps={{ sx: { color: "inherit" } }}
                      label={<>   Total Distance</>}
                      value={format(focusedChain?.totalDistanceTravelled, {
                        suffix: "km",
                      })}
                    />&emsp;&emsp;&emsp;

                    <Value
                      mb={0}
                      noWrap
                      LabelProps={{ sx: { color: "inherit" } }}
                      label={<>&#8709; Speed</>}
                      value={format(focusedChain?.averageChainSpeed, {
                        suffix: "km/year",
                      })}
                    />

                  </Box>









                  <Box
                    sx={{
                      ml: 4,
                      px: 1.5,
                      borderRadius: 1,
                      boxShadow: 2,
                      bgcolor: focusedChain?.fillColor,
                      color: focusedChain?.textColor,
                      opacity: 0.75,
                      height: 40, // this is the height of the button next to it...
                      display: "flex",
                      alignItems: "center",
                      overflow: "auto",
                    }}
                  >

                    <Value
                      mb={0}
                      noWrap
                      sx={{ mr: 4 }}
                      LabelProps={{ sx: { color: "inherit" } }}
                      label={<>&#8709; Publishing interval </>}
                      value={format(focusedChain?.frequency, {
                        suffix: "years",
                      })}
                    />
                    <Value
                      mb={0}
                      noWrap
                      sx={{ mr: 4 }}
                      LabelProps={{ sx: { color: "inherit" } }}
                      label={<>&#8709; Frequency Output </>}
                      value={format(
                        focusedChain?.speedOfSpreadRelativeToPublishingInterval,
                        { suffix: "edition/year per interval" }
                      )}
                    />
                    {focusedChain?.isOutgoing && (
                      <Value
                        mb={0}
                        noWrap
                        LabelProps={{ sx: { color: "inherit" } }}
                        label={<>&#8709; Mean Speed</>}
                        value={format(focusedChain?.speedOfSpread, {
                          suffix: "km/year per interval",
                        })}
                      />


                    )}
                  </Box>








                </div>
              </Fade>
            </Box>
          </Box>
        </Box>
        <Slide direction="right" in={isOpen} mountOnEnter unmountOnExit>
          <Box
            sx={{
              position: "relative",
              width: MAX_WIDTH,
              bgcolor: "background.paper",
              boxShadow: 2,
              borderRadius: 1,
              pointerEvents: "auto",
              display: "flex",
              overflow: "auto",
              flexDirection: "column",
              height: "100%",
              p: 2,
            }}
          >
            <Tooltip title="Deselect Edition" placement="top" enterDelay={300}>
              <IconButton
                onClick={handleClickCloseMetaButton}
                size="small"
                sx={{ position: "absolute", top: 4, right: 4 }}
              >
                <CloseIcon fontSize="small" />
              </IconButton>
            </Tooltip>
            <Box>
              <Divider sx={{ mt: 1, mb: 1 }} textAlign="left">
                <Typography variant="h6">Edition</Typography>
              </Divider>
            </Box>
            <Box
              sx={{ mb: 1 }}
              display="flex"
              alignItems="center"
              justifyContent="space-between"
            >
              <IconButton size="small" onClick={handleClickPrev}>
                <ArrowBackIosNewIcon fontSize="small" color="primary" />
              </IconButton>
              <Typography
                variant="body1"
                sx={{
                  textAlign: "center",
                }}
              >
                {editionIndex + 1}/{focusedChain?.flattend.length}
              </Typography>
              <IconButton size="small" onClick={handleClickNext}>
                <ArrowForwardIosIcon fontSize="small" color="primary" />
              </IconButton>
            </Box>
            <Box sx={{ flexGrow: 1, overflow: "auto" }}>
              <EditionMeta
                edition={focusedEdition}
                fingerprint={chains.fingerprintForEdition(focusedEdition)}
              />
            </Box>
            <Button
              sx={{ mt: 1 }}
              href={focusedEdition?.href}
              target="_blank"
              fullWidth
              startIcon={<InsertLinkIcon />}
            >
              Link
            </Button>
          </Box>
        </Slide>
      </Box>
      <Box
        sx={{
          pointerEvents: "none",
          position: "fixed",
          bottom: 8,
          left: 32,
          zIndex: 1,
        }}
      >
        <Typography variant="caption" color="text.secondary" sx={{ mr: 4 }}>
          De Sphaera Project
        </Typography>
        <Typography variant="caption" color="text.secondary" sx={{ mr: 4 }}>
          Supported by the German Ministry for Education and Research as BIFOLD
        </Typography>
        <Typography variant="caption" color="text.secondary" sx={{ mr: 4 }}>
          Max Planck Institute for the History of Science
        </Typography>
      </Box>
    </>
  );
};

export default observer(Header);
