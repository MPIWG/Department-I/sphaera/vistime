import React from "react";
import { Box, Typography } from "@mui/material";
import format from "../utils/format";

export const Value = (props) => {
  const { value, label, noWrap, LabelProps, ValueProps, ...rest } = props;
  return (
    <Box mb={0.7} {...rest}>
      <Typography
        variant="overline"
        color="text.secondary"
        {...LabelProps}
        sx={{ whiteSpace: noWrap ? "nowrap" : "normal", ...LabelProps.sx }}
      >
        {label}
      </Typography>
      <Typography
        variant="body2"
        component="div"
        {...ValueProps}
        sx={{ whiteSpace: noWrap ? "nowrap" : "normal", ...LabelProps.sx }}
      >
        {value}
      </Typography>
    </Box>
  );
};

Value.defaultProps = {
  noWrap: false,
  LabelProps: {},
  ValueProps: {},
};

const splitNames = (s) => s.split(";").join(" | ");

const EditionMeta = (props) => {
  const { edition, fingerprint } = props;
  if (!edition) return null;
  return (
    <>
      <Value label="Title" value={edition.label} />
      <Value label="Location" value={edition.place.label} />
      <Value label="Year" value={edition.yearPublished} />
      <Value label="Author" value={splitNames(edition.author)} />
      <Value label="Publisher" value={splitNames(edition.publisher)} />
      <Value label="Printer" value={splitNames(edition.printer)} />
      <Value label="Format" value={edition.format} />
      <Value label="Fingerprint" value={edition.fingerprint} />
      {fingerprint && (
        <>
          <Value
            label={<>Interval Frequency Output</>}
            value={format(
              fingerprint?.speedOfSpreadRelativeToPublishingInterval,
              { suffix: "edition/year" }
            )}
          />
          {fingerprint.isOutgoing && (
            <Value
              label={<>Interval Speed</>}
              value={format(fingerprint?.speedOfSpread, { suffix: "km/year" })}
            />
          )}
        </>
      )}
    </>
  );
};

export default EditionMeta;
