import * as React from "react";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import Popper from "@mui/material/Popper";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import Fab from "@mui/material/Fab";
import { styled } from "@mui/material/styles";
import { observer } from "mobx-react";
import { useStores } from "../stores/";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import ArrowRightIcon from "@mui/icons-material/ArrowRight";
const MAX_WIDTH = 248;

const StyledFab = styled(Fab)((props, theme) => ({
  boxShadow: "none",
  background: props.isSelected
    ? props.theme.palette.common.greyMain
    : props.chain.fillColor,
  borderStyle: "solid",
  borderWidth: 1,
  borderColor: "transparent",
  color: props.isSelected
    ? props.theme.palette.common.white
    : props.chain.textColor,
  "&:hover": {
    background: "transparent",
    borderColor: props.theme.palette.primary.main,
    color: props.theme.palette.primary.main,
  },
}));

function ChainFilter() {
  const { dataStore, uiStore } = useStores();
  const { chains } = dataStore;
  const { excludedChains, focusedChain } = uiStore;
  const [anchorEl, setAnchorEl] = React.useState(null);

  const toggleAnchorElement = (e) => {
    if (anchorEl) {
      setAnchorEl(null);
    } else {
      setAnchorEl(e.currentTarget);
    }
  };

  const handleClickButton = (event) => {
    toggleAnchorElement(event);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const createClickChainButtonHandler = (chain) => () => {
    uiStore.toggleChainExclusion(chain);
  };
  const handleClickSelectAll = () => {
    uiStore.includeAllChains();
  };
  const handleClickDeselectAll = () => {
    uiStore.excludeAllChains();
  };

  const numChains = chains.asList.length;
  const numExcludedChains = excludedChains.length;
  const numSelectedChains = Math.abs(numChains - numExcludedChains);
  const isChainFilterActive = numChains !== numSelectedChains;
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <>
      <Button
        sx={{
          minWidth: MAX_WIDTH,
          height: 40,
          justifyContent: "space-between",
        }}
        endIcon={
          <Box sx={{ display: "flex", alignItems: "center" }}>
            <Typography variant="caption" sx={{ mr: 0.5, lineHeight: 1 }}>
              {isChainFilterActive && `${numSelectedChains}/`}
              {numChains} selected
            </Typography>
            {open ? <ArrowDropDownIcon /> : <ArrowRightIcon />}
          </Box>
        }
        aria-describedby={id}
        variant="contained"
        color="secondary"
        onClick={handleClickButton}
      >
        Filter Chains
      </Button>
      <Popper id={id} open={open} anchorEl={anchorEl} onClose={handleClose}>
        <ClickAwayListener onClickAway={handleClose}>
          <Box
            sx={{
              display: "flex",
              flexWrap: "wrap",
              justifyContent: "center",
              width: MAX_WIDTH,
              my: 2,
              borderRadius: 1,
              boxShadow: 2,
              backgroundColor: "background.paper",
              py: 1,
            }}
          >
            {chains.asList.map((chain) => {
              const isSelected = excludedChains.includes(chain);
              return (
                <StyledFab
                  key={chain.index}
                  onClick={createClickChainButtonHandler(chain)}
                  size="small"
                  chain={chain}
                  sx={{
                    m: 0.5,
                  }}
                  isSelected={isSelected}
                >
                  <Typography variant="small">{chain.index + 1}</Typography>
                </StyledFab>
              );
            })}
            <Box
              sx={{
                display: "flex",
                justifyContent: "flex-start",
                width: "100%",
                px: 1,
                mt: 0.5,
              }}
            >
              <Button size="small" onClick={handleClickSelectAll}>
                Select All
              </Button>
              <Divider
                orientation="vertical"
                variant="middle"
                flexItem
                sx={{ mx: 0.5 }}
              />
              <Button size="small" onClick={handleClickDeselectAll}>
                Deselect All
              </Button>
            </Box>
          </Box>
        </ClickAwayListener>
      </Popper>
    </>
  );
}

export default observer(ChainFilter);
