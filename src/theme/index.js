import { createTheme } from "@mui/material/styles";
import { red } from "@mui/material/colors";
import { upperCase } from "lodash";

const GREY_DARK = "#878278";
const GREY_MAIN = "#C3BEB9";
const GREY_LIGHT = "#E1E1DC";
const BACKGROUND = "#F5F3EF";

const PRIMARY_LIGHT = "#006464";
const PRIMARY_DARK = "#659078";
export const PRIMARY_MAIN = "#004646";

const SECONDARY_MAIN = "#50FA96";

const SERIF = ["'Alegreya'", "Spectra Book", "Times", "Vollkorn", "serif"].join(
  " ,"
);
const SANS = [
  "'HKGrotesk'",
  "Apercu Pro",
  "Helvetica",
  "Roboto",
  "sans-serif",
].join(" ,");

const FONTWEIGHT_REGULAR = 400;
const FONTWEIGHT_MEDIUM = 500;
const FONTWEIGHT_BOLD = 700;

// Create a theme instance.
const theme = createTheme({
  palette: {
    primary: {
      main: PRIMARY_MAIN,
    },
    secondary: {
      main: SECONDARY_MAIN,
    },
    error: {
      main: red.A400,
    },
    background: {
      paper: BACKGROUND,
    },
    common: {
      greyDark: GREY_DARK,
      greyLight: GREY_LIGHT,
      greyMain: GREY_MAIN,
    },
    text: {
      primary: PRIMARY_MAIN,
      secondary: GREY_DARK,
    },
  },

  typography: {
    fontFamily: SANS,
    fontWeightRegular: FONTWEIGHT_REGULAR,
    fontWeightMedium: FONTWEIGHT_MEDIUM,
    fontWeightBold: FONTWEIGHT_BOLD,
    h1: {
      fontFamily: SERIF,
    },
    h2: {
      fontSize: 34,
      fontFamily: SERIF,
    },
    h3: {
      fontSize: 18,
      fontFamily: SERIF,
    },
    h4: {
      fontSize: 25,
    },
    h6: {
      fontSize: 21,
      fontWeight: "800",
    },
    body1: {
      fontSize: 14,
    },
    body2: {
      fontSize: 12,
      lineHeight: 1.2,
    },
    button: {
      fontSize: 14,
      textTransform: "none",
    },
    small: {},
    overline: {
      lineHeight: 1,
      fontSize: 13,
      letterSpacing: 0.35,
      fontWeight: FONTWEIGHT_BOLD,
    },
    caption: {},
  },
});

theme.components = {
  MuiFab: {
    defaultProps: {},
    styleOverrides: {
      root: {},
      primary: {},
      sizeLarge: {
        width: 40,
        height: 40,
      },
      sizeSmall: {
        width: 22,
        height: 22,
        minHeight: 22,
        fontSize: 11,
      },
    },
  },
  MuiButton: {
    styleOverrides: {
      primary: {
        fontSize: 24,
        textTransform: "none",
        color: PRIMARY_MAIN,
      },
      containedSecondary: {
        color: PRIMARY_MAIN,
        backgroundColor: BACKGROUND,
        color: PRIMARY_MAIN,
        "&:hover": {
          backgroundColor: GREY_LIGHT,
          color: PRIMARY_LIGHT,
        },
      },
    },
  },
  MuiTooltip: {
    styleOverrides: {
      tooltip: {
        boxShadow: theme.shadows[2],
        color: PRIMARY_MAIN,
        background: BACKGROUND,
      },
      arrow: {
        color: PRIMARY_MAIN,
      },
    },
  },
  MuiSpeedDialAction: {
    styleOverrides: {
      fab: {},
    },
  },
};

export default theme;
